
Shader "Low Poly Water"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
		_WaterColor("Water Color", Color) = (0.4926471,0.8740366,1,1)
		_WaveGuide("Wave Guide", 2D) = "white" {}
		_WaveSpeed("Wave Speed", Range( 0 , 5)) = 0
		_WaveHeight("Wave Height", Range( 0 , 5)) = 0
		_FoamColor("Foam Color", Color) = (1,1,1,0)
		_Foam("Foam", 2D) = "white" {}
		_FoamDistortion("Foam Distortion", 2D) = "white" {}
		_FoamDist("Foam Dist", Range( 0 , 1)) = 0.1
		_Opacity("Opacity", Range( 0 , 1)) = 0
		[Toggle]_LowPoly("Low Poly", Float) = 1
		_NormalOnlyNoPolyMode("Normal (Only No Poly Mode)", 2D) = "bump" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Off
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#pragma target 3.0
		#pragma surface surf Standard alpha:fade keepalpha noshadow vertex:vertexDataFunc 
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
			float4 screenPos;
			float2 texcoord_0;
		};

		uniform float _LowPoly;
		uniform sampler2D _NormalOnlyNoPolyMode;
		uniform float4 _NormalOnlyNoPolyMode_ST;
		uniform float4 _WaterColor;
		uniform float4 _FoamColor;
		uniform sampler2D _Foam;
		uniform float _WaveSpeed;
		uniform float4 _Foam_ST;
		uniform sampler2D _FoamDistortion;
		uniform sampler2D _CameraDepthTexture;
		uniform float _FoamDist;
		uniform float _Opacity;
		uniform sampler2D _WaveGuide;
		uniform float _WaveHeight;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float4 speed = ( _Time * _WaveSpeed );
			float componentMask118 = v.vertex.xyz.y;
			o.texcoord_0.xy = v.texcoord.xy * float2( 1,1 ) + ( speed + componentMask118 ).xy;
			float3 VertexAnimation = ( ( tex2Dlod( _WaveGuide, float4( o.texcoord_0, 0.0 , 0.0 ) ).r - 0.5 ) * ( v.normal * _WaveHeight ) );
			v.vertex.xyz += VertexAnimation;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_NormalOnlyNoPolyMode = i.uv_texcoord * _NormalOnlyNoPolyMode_ST.xy + _NormalOnlyNoPolyMode_ST.zw;
			float3 ase_worldPos = i.worldPos;
			float3 Normal = lerp(UnpackNormal( tex2D( _NormalOnlyNoPolyMode, uv_NormalOnlyNoPolyMode ) ),normalize( ( cross( ddx( ase_worldPos ) , ddy( ase_worldPos ) ) + float3( 1E-09,0,0 ) ) ),_LowPoly);
			o.Normal = Normal;
			float4 Albedo = _WaterColor;
			o.Albedo = Albedo.rgb;
			float4 speed = ( _Time * _WaveSpeed );
			float2 uv_Foam = i.uv_texcoord * _Foam_ST.xy + _Foam_ST.zw;
			float cos182 = cos( speed );
			float sin182 = sin( speed );
			float2 rotator182 = mul((abs( uv_Foam+speed.x * float2(0.5,0.5 ))) - float2( 0,0 ), float2x2(cos182,-sin182,sin182,cos182)) + float2( 0,0 );
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float screenDepth164 = LinearEyeDepth(UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture,UNITY_PROJ_COORD(ase_screenPos))));
			float distanceDepth164 = abs( ( screenDepth164 - LinearEyeDepth( ase_screenPos.z/ ase_screenPos.w ) ) / _FoamDist );
			float4 Emission = lerp( ( _FoamColor * tex2D( _Foam, rotator182 ) ) , float4(0,0,0,0) , clamp( ( clamp( tex2D( _FoamDistortion, rotator182 ).r , 0.0 , 1.0 ) * distanceDepth164 ) , 0.0 , 1.0 ) );
			o.Emission = Emission.rgb;
			o.Alpha = _Opacity;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
