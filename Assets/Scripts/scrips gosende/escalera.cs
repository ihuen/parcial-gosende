using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class escalera : MonoBehaviour
{
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            float verticalInput = Input.GetAxis("Vertical");
            Rigidbody rb = other.GetComponent<Rigidbody>();
            rb.velocity = new Vector3(rb.velocity.x, verticalInput * 5f, 0); // Ajusta la velocidad de escalada
        }
    }
}
