using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controljugador : MonoBehaviour
{
    public float moveSpeed = 5f;
    public float jumpForce = 10f;
    public float dashSpeed = 20f;
    public float dashDuration = 0.2f;
    public float dashCooldown = 1f;

    private Rigidbody rb;
    private bool isGrounded;
    private bool isDashing;
    private float dashTimeLeft;
    private float lastDash = -100f;

    public Transform groundCheck;
    public LayerMask groundLayer;
    public float checkRadius = 0.2f;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (!isDashing)
        {
            Move();
            Jump();
            CheckGround();
        }

        if (Input.GetKeyDown(KeyCode.LeftShift) && Time.time >= (lastDash + dashCooldown))
        {
            StartDash();
        }

        if (isDashing)
        {
            ContinueDash();
        }
    }

    private void Move()
    {
        float moveInput = Input.GetAxis("Horizontal");
        if (!isDashing)
        {
            rb.velocity = new Vector3(moveInput * moveSpeed, rb.velocity.y, 0);
        }

        // Flip the character sprite based on movement direction
        if (moveInput > 0)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        else if (moveInput < 0)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
    }

    private void Jump()
    {
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            rb.velocity = new Vector3(rb.velocity.x, jumpForce, 0);
        }
    }

    private void CheckGround()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, checkRadius, groundLayer);
    }

    private void StartDash()
    {
        isDashing = true;
        dashTimeLeft = dashDuration;
        lastDash = Time.time;
        rb.velocity = new Vector3(transform.localScale.x * dashSpeed, rb.velocity.y, 0);
    }

    private void ContinueDash()
    {
        if (dashTimeLeft > 0)
        {
            dashTimeLeft -= Time.deltaTime;
            rb.velocity = new Vector3(transform.localScale.x * dashSpeed, rb.velocity.y, 0);
        }
        else
        {
            isDashing = false;
        }
    }
}
