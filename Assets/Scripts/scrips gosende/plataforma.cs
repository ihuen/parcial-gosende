using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plataforma : MonoBehaviour
{
    public Transform pointA;
    public Transform pointB;
    public float speed = 2f;

    private Vector3 target;

    private void Start()
    {
        // Establecer el objetivo inicial a pointB
        target = pointB.position;
    }

    private void Update()
    {
        // Mover la plataforma hacia el objetivo
        transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);

        // Verificar si la plataforma est� lo suficientemente cerca del objetivo para cambiar de direcci�n
        if (Vector3.Distance(transform.position, target) < 0.1f)
        {
            // Cambiar el objetivo al otro punto
            target = (target == pointA.position) ? pointB.position : pointA.position;
        }
    }
}
