using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deslisporapared : MonoBehaviour
{
    public float slideSpeed = 2f;
    public float wallJumpForce = 10f;
    private bool isSliding = false;

    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Wall") && !isGrounded())
        {
            isSliding = true;
            rb.velocity = new Vector3(rb.velocity.x, -slideSpeed, rb.velocity.z);

            if (Input.GetButtonDown("Jump"))
            {
                rb.velocity = new Vector3(wallJumpForce * (collision.contacts[0].normal.x > 0 ? -1 : 1), wallJumpForce, rb.velocity.z);
                isSliding = false;
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            isSliding = false;
        }
    }

    private bool isGrounded()
    {
        // M�todo para comprobar si el jugador est� en el suelo
        return Physics.Raycast(transform.position, Vector3.down, 0.1f);
    }
}
